# Dr. Mapzo free maps (unofficial)

Unofficial preparation of Dr. Mapzo's free maps for use as Scenes in Foundry VTT.
Please see their Patreon page here for more: https://www.patreon.com/drmapzo

All maps are 72 pixels-per-inch/-square.
Most/all of these have built-in support for the Dancing Lights and Wall Height modules, if you have them installed.

## How to use
In Foundry VTT, use this module installation URL:
https://gitlab.com/Akaito1/foundry-drmapzo-free/-/raw/master/module.json

## Map highlights
- Volcanic Castle Interior
  I think this showcases the best way to mix the fancy perspective bits of the
  map and strictly top-down parts to keep everything functional while still
  looking nice.
- Haunted Mansion
  If you have the Multilevel Tokens mod, this map will allow players to move
  between floors of the mansion on their own.  And see tokens on the grounds
  outside from the upper windows.
- Greenfire Ruins
  Some neat lighting going on with the small channels of green stuff.

## TODO
### First pass
- [ ] 105. Cemetary
- [x] 104. Underground Chamber
- [ ] 103. Snowy Camp
- [x] 102. Sandy Island
- [x] 101. Sky Islands
- [x] 100. Haunted Mansion
- [x] 99. The Path of the Hanged
      Skip!  No free version.
- [ ] 98. List Field
- [x] 97. Greenfire Ruins
- [x] 96. Courtroom
- [x] 95. Forest Spring
- [x] 94. Mountainous Path
      Skip!  No free version.
- [x] 93. Royal Bedroom
- [x] 92. Goblin Camp
- [x] 91. Dungeon Crypt
- [x] 90. Coastal Cliff
- [x] 89. Autumnal Forest
      Skip!  No free version.
- [x] 88. Muddy Forest
- [x] 87. Dining Room
- [x] 86. Gnome Village
- [x] 85. Alleyways
- [x] 84. Rocky Dungeon
- [x] 83. Winter Village
- [x] 82. Forest Ruins
- [x] 81. Marshland
- [x] 80. Theater Stage
      Skip!  No free version.
- [x] 79. Volcanic Castle Interior
- [ ] 78. Volcanic Castle Walls
- [x] 77. Volcanic Path
- [ ] 76. Fortress Forest Path
- [ ] 75. War Airship
- [ ] 74. Carnivorous Swamp
- [ ] 73. Abandoned Wagon
- [ ] 72. Training Grounds
- [x] 71. Cave Entrance
      Skip!  No free version.
- [ ] 70. Gallows
- [ ] 69. Lush Cavern
- [ ] 68. Potion Shop
- [ ] 67. Docks
- [x] 66. Armory
      Skip!  No free version.
- [ ] 65. Forgotten Caverns
- [ ] 64. Oasis
- [ ] 63. Rocky Forest
- [ ] 62. Island Ruins
- [ ] 61. Public Bath
- [ ] 60. Behold the Tarrasque!
- [ ] 59. Ice Throne Room
- [ ] 58. Frozen Path
- [ ] 57. Festive Tavern
- [ ] 56. Lagoon
- [ ] 55. Dragon Nest
- [ ] 54. Dwarven Mining Village
- [ ] 53. Glacial Cave
- [x] 52. Barn
      Skip!  No free version.
- [ ] 51. Vampire Mausoleum
- [ ] 50. Ship Graveyard
- [ ] 49. River Canyon
- [ ] 48. Jungle
- [ ] 47. Magical Castle
- [ ] 46. Mountain Village
- [ ] 45. Underground Magic Portals
- [ ] 44. Sewers
- [x] 43. Ghostly Cave
      Skip!  No free version.
- [ ] 42. Mushroom Cave
- [ ] 41. Desert Ruins
- [ ] 40. Crystal Cave
- [ ] 39. Forest Shrine
- [ ] 38. High Bridges
- [ ] 37. Wildfire
- [ ] 36. Floating Island
- [ ] 35. River Crossing
- [ ] 34. Snowy Hill
- [ ] 33. Dunes
- [x] 32. Rift Dungeon
      Skip!  No free version.
- [ ] 31. Scary Forest
- [ ] 30. Sacrifice Tower
- [ ] 29. Church
- [ ] 28. Catacombs
- [ ] 27. Flooded Cave
- [ ] 26. Castle Entrance
- [ ] 25. Elf Village
- [ ] 24. Fort Ruins
- [ ] 23. Treasure Island
- [ ] 22. Labyrinthine Dungeon
- [ ] 21. Magic Library
- [ ] 20. Wizard's Cabin
- [ ] 19. Enchanted Forest
- [ ] 18. Bandit Hideout
- [ ] 17. Small Farming Village
- [ ] 16. Ancient Dungeon
- [ ] 15. Island
- [ ] 14. Earth Dungeon
- [ ] 13. River Crossing
- [ ] 12. Inn
- [ ] 11. Volcanic Wilderness
- [ ] 10. Dungeon And Cavern
- [ ] 9. Town
- [ ] 8. Forest and River
- [ ] 7. Forest Islands
- [ ] 6. Ruins In The Forest
- [ ] 5. Snowy Mountain
- [ ] 4. Cavernous Dungeon
- [ ] 3. Campsite By The Lake
- [ ] 2. Sarcophagus Room
- [ ] 1. Graveyard

### Second pass
- [ ] Redo Coastal Cliffs walls.
- [ ] Cleanup Goblin Camp walls.
- [ ] Add Dancing Lights support for maps 83-92?
- [ ] Improve Volcanic Path map with support for the Wall Height module.

